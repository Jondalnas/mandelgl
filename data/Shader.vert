uniform mat4 transform;
uniform mat3 normalMatrix;
uniform vec3 lightNormal;

attribute vec4 position;
attribute vec4 color;
attribute vec3 normal;

uniform vec4 size;
uniform vec2 screenSize;

varying vec2 pos;

void main() {
	pos = vec2((position.x/screenSize.x+0.5)*(size.z-size.x)+size.x, (position.y/screenSize.y+0.5)*(size.w-size.y)+size.y);
	gl_Position = transform * position;
}
