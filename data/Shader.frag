varying vec2 pos;

int MAX_ITERATION = 500;

void main() {
	float x = 0;
	float y = 0;
	
	int ittr = 0;
	
	vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
	
	for (; ittr < MAX_ITERATION; ittr++) {
		float xx = x * x;
		float yy = y * y;
		float xtmp = xx - yy + pos.x;
		y = 2 * x * y + pos.y;
		x = xtmp;
		
		if (xx + yy >= 2*2) {
			//float col = ittr/float(MAX_ITERATION);// + 1 - log2(log(sqrt(xx+yy)));
			
			float hue = float(ittr)/float(MAX_ITERATION);
			
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(vec3(1.0, 1.0, 1.0)*hue + K.xyz) * 6.0 - K.www);
			
			color = vec4(clamp(p - K.xxx, 0.0, 1.0), 1.0);
			
			break;
		}
	}
	
	gl_FragColor = color;
}