PShader fractal;

double[] fractalCorners = {-2.5, -1, 1, 1};

void setup() {
  fullScreen(P3D);
  noStroke();
  fractal = loadShader("Shader.frag", "Shader.vert");
  fractal.set("screenSize", float(width), float(height));
  fractal.set("size", (float) fractalCorners[0], (float) fractalCorners[1], (float) fractalCorners[2], (float) fractalCorners[3]);
}

void draw() {
  if (mousePressed) {
    if (mouseButton == 37)
      zoomToMouse(0.99);
    else if (mouseButton == 39)
      zoomToMouse(1/0.99);
  }
  
  shader(fractal);
  
  rect(0, 0, width, height);
}

void zoomToPoint(double x, double y, double zoom) {
  double currW = fractalCorners[2] - fractalCorners[0];
  double currH = fractalCorners[3] - fractalCorners[1];
  
  currW *= zoom;
  currH *= zoom;
  
  fractalCorners[0] = x - currW/2.0;
  fractalCorners[1] = y - currH/2.0;
  fractalCorners[2] = x + currW/2.0;
  fractalCorners[3] = y + currH/2.0;
  
  fractal.set("size", (float) fractalCorners[0], (float) fractalCorners[1], (float) fractalCorners[2], (float) fractalCorners[3]);
}

void zoomToMouse(double zoom) {
  double currW = fractalCorners[2] - fractalCorners[0];
  double currH = fractalCorners[3] - fractalCorners[1];
  
  double mx = ((double) mouseX/(double) width)*currW + fractalCorners[0];
  double my = ((double) mouseY/(double) height)*currH + fractalCorners[1];
  
  currW *= zoom;
  currH *= zoom;
  
  fractalCorners[0] = mx - currW/2.0 - ((double) mouseX/(double) width-0.5)*currW;
  fractalCorners[1] = my - currH/2.0 - ((double) mouseY/(double) height-0.5)*currH;
  fractalCorners[2] = mx + currW/2.0 - ((double) mouseX/(double) width-0.5)*currW;
  fractalCorners[3] = my + currH/2.0 - ((double) mouseY/(double) height-0.5)*currH;
  
  fractal.set("size", (float) fractalCorners[0], (float) fractalCorners[1], (float) fractalCorners[2], (float) fractalCorners[3]);
}
